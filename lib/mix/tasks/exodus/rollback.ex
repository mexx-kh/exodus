defmodule Mix.Tasks.Exodus.Rollback do
  use Exodus.Task
  @shortdoc "Run Exodus migrations"

  def run(_) do
    execute fn(app) ->
      Exodus.Rollback.all(Exodus.Config.new(app))
    end
  end
end

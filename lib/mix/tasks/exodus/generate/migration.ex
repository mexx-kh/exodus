defmodule Mix.Tasks.Exodus.Generate.Migration do
  use Mix.Task

  @shortdoc "Generate Exodus migrations"

  def run([name | _]) do
    if Mix.Project.umbrella? do
      Mix.raise("Can't run this task in an umbrella root. You need to be inside an application")
    end

    config = Exodus.Config.new(Mix.Project.config()[:app])

    %{}
    |> with_name(name)
    |> with_base_content(config)
    |> with_timestamp()
    |> create!(config)
  end
  def run(_), do: usage_instructions()

  defp usage_instructions do
    Mix.raise("You need to provide the name of the migration")
  end

  defp with_name(params, name) do
    Map.put(params, :name, name)
  end

  defp with_base_content(%{name: name} = params, config) do
    content =
      """
      defmodule #{module_name(name, config)} do
        use #{config.namespace}

        def up do
        end

        def down do
        end
      end
      """

    Map.put(params, :content, content)
  end
  defp with_base_content(params, _) do
    Map.put(params, :content, "")
  end

  defp with_timestamp(params) do
    time =
      DateTime.utc_now 
      |> DateTime.to_unix

    Map.put(params, :time, time)
  end

  defp module_name(name, config) do
    name =
      name
      |> String.split("_")
      |> Enum.map(&String.capitalize/1)
      |> Enum.join("")

    "#{config.namespace}.#{name}"
  end

  defp create!(%{name: name, time: time, content: content}, config) do
    filename = "#{directory(config)}/#{time}_#{name}.ex"
    File.write!(filename, content)

    Mix.Shell.IO.info("Generated migration #{filename}")
  end

  defp directory(config) do
    if not File.dir?(config.migrations_path) do
      File.mkdir_p!(config.migrations_path)
    end
    config.migrations_path
  end
end

defmodule Mix.Tasks.Exodus.Migrate do
  use Exodus.Task
  @shortdoc "Run Exodus migrations"

  def run(_) do
    execute fn(app) ->
      Exodus.Migrate.all(Exodus.Config.new(app))
    end
  end
end

defmodule Exodus.Migrations.Pending do
  @spec load(Exodus.Migrations.t) :: [String.t]
  def load(struct) do
    struct.migrations
    |> Enum.reject(fn({version, _}) ->
      Enum.member?(struct.migrated, version)
    end)
    |> Enum.map(fn({version, _}) -> version end)
  end
end

defmodule Exodus.Migrations.History do
  alias NimbleCSV.RFC4180, as: CSV

  @spec load(Exodus.Config.t) :: [String.t]
  def load(config) do
    config.versions_path()
    |> File.read()
    |> case do
      {:ok, csv} ->
        csv
        |> CSV.parse_string()
        |> List.flatten()
      _ -> []
    end
  end

  @spec migrated(String.t, Exodus.Config.t) :: :no_return
  def migrated(version, config) do
    ["versions" | versions_with(version, config)]
    |> Enum.join("\n")
    |> save(config)
  end

  @spec rolled_back(String.t, Exodus.Config.t) :: :no_return
  def rolled_back(version, config) do
    ["versions" | versions_without(version, config)]
    |> Enum.join("\n")
    |> save(config)
  end

  defp versions_with(version, config) do
    [version | load(config)]
    |> sort_versions()
  end

  defp versions_without(version, config) do
    load(config) -- [version]
    |> sort_versions()
  end

  defp sort_versions(versions) do
    versions
    |> Enum.uniq()
    |> Enum.sort()
  end

  defp save(versions, config) do
    config.versions_path()
    |> File.write(versions)
  end
end

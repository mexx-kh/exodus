defmodule Exodus.Migration do
  defmacro __using__(_) do
    quote do
      @behaviour Exodus.Migration
    end
  end

  @callback up :: :ok | {:error, String.t}
  @callback down :: :ok | {:error, String.t}

  @optional_callbacks down: 0
end

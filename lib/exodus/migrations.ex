defmodule Exodus.Migrations do
  alias Exodus.Migrations.History
  alias Exodus.Migrations.Pending

  defstruct pending: [], migrated: [], migrations: %{}

  def load(config) do
    %__MODULE__{}
    |> load_migrations(config)
    |> load_migrated(config)
    |> load_pending()
  end

  defp load_migrations(struct, config) do
    migrations =
      config.migrations_path()
      |> File.ls()
      |> case do
        {:ok, migrations} -> parse_versions(migrations)
        {:error, _} -> %{}
      end

    %{struct | migrations: migrations}
  end

  defp load_migrated(struct, config) do
    %{struct | migrated: History.load(config)}
  end

  defp load_pending(struct) do
    %{struct | pending: Pending.load(struct)}
  end

  defp parse_versions([]), do: []
  defp parse_versions(migrations) do
    Enum.map(migrations, fn(migration) ->
      case Regex.named_captures(~r/(?<version>\d*)_(?<name>.*)\.ex/, migration) do
        %{"name" => name, "version" => version} -> {version, %{name: name, file: migration}}
        _ -> nil
      end
    end)
    |> Enum.reject(&is_nil/1)
    |> Enum.into(%{})
  end
end

defmodule Exodus.Migrate do
  alias Exodus.Migrations
  alias Exodus.Migrations.History

  @spec all(Exodus.Config.t) :: :ok | {:error, String.t}
  def all(config) do
    migrations = Migrations.load(config)
    migrations.pending
    |> Enum.each(fn(version) ->
      run!(version, migrations.migrations[version], config)
      History.migrated(version, config)
    end)
  end

  defp run!(_, %{name: name, file: file}, config) do
    module =
      name
      |> String.split("_")
      |> Enum.map(&String.capitalize/1)
      |> Enum.join("")
      |> append_namespace(config)

    Code.require_file("#{config.migrations_path}/#{file}")
    Mix.Shell.IO.info("Running migration #{file}")

    apply(module, :up, [])
    |> case do
      :ok ->
        Mix.Shell.IO.info("-> Migrated #{file}")
      {:error, error} ->
        Mix.raise("Error migrating #{file}: #{inspect(error)}")
    end
  end

  defp append_namespace(name, config) do
    Module.concat(config.namespace, name)
  end
end

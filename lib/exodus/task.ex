defmodule Exodus.Task do
  defmacro __using__(_) do
    quote do
      use Mix.Task
      import Exodus.Task
    end
  end

  defmacro execute(migration) do
    quote do
      Mix.Task.run("compile")
      if Mix.Project.umbrella?() do
        Mix.Project.apps_paths()
        |> Enum.each(fn({app, _}) ->
          unquote(migration).(app)
        end)
      else
        unquote(migration).(:exodus)
      end
    end
  end
end

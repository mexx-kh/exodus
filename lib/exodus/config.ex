defmodule Exodus.Config do
  defstruct migrations_path: "priv/exodus/migrations",
            versions_path:   "priv/exodus/versions.csv",
            namespace:       "Exodus.Migration"

  def new(app \\ :exodus) do
    Application.get_env(app, :exodus, default_config(app))
  end

  defp default_config(:exodus), do: %__MODULE__{}
  defp default_config(app) do
    app_path = Mix.Project.apps_paths()[app]
    config   = %__MODULE__{}

    %__MODULE__{
      migrations_path: join_path(app_path, config.migrations_path),
      versions_path: join_path(app_path, config.versions_path)
    }
  end

  defp join_path(nil, b), do: b
  defp join_path(a, b), do: Path.join(a, b)
end

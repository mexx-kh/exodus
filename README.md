# Exodus

Simple package to handle generic-non-ecto migrations

## Usage

### Generating migration files

```
$ mix exodus.generate.migration name_of_the_migration_here
Generated migration priv/exodus/migrations/1510927184_name_of_the_migration_here.ex
```

Migrations files will be generate in `priv/exodus/migrations/` in the format `TIMESTAMP_name_of_the_migration_here.ex`.

If `priv/exodus/migrations/` does not exist, it's going to be created before creating the migration.

### Running migrations

```
$ mix exodus.migrate
Running migration 1510927184_name_of_the_migration_here.ex
-> Migrated 1510927184_name_of_the_migration_here.ex
```

This will run all pending migrations in ascending order by timestamp. If there's a failure in one migration an error will be raised and migrations will be stopped.

### Rolling back migrations

```
$ mix exodus.rollback
Rolling back migration 1510927184_name_of_the_migration_here.ex
-> Rolled back 1510927184_name_of_the_migration_here.ex
```

This will rollback all migrations that were run in descending order by timestamp. If there's a failure in one migration an error will be raised and migrations will be stopped.

## Behind the scenes

Migrations are controlled using a CSV file that sits on `priv/exodus/versions.csv`.

The file contains only one column `versions` which will have the timestamp of all migrations that were already run in ascending order.

This file is only created after you run the migrations for the first time and **you should add it to your `.gitignore`**.

When a migration is run, the `versions.csv` file is updated with the the timestamp of that migration.

When a migration is rolled back, the `versions.csv` file is updated to remove the timestamp of that migration.

## Roadmap

- Add support to migrate/rollback one single migration
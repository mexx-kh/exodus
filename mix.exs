defmodule Exodus.Mixfile do
  use Mix.Project

  def project do
    [
      app: :exodus,
      version: "0.2.0",
      elixir: "~> 1.5",
      start_permanent: Mix.env == :prod,
      aliases: aliases(),
      deps: deps()
    ]
  end

  def application do
    [
      extra_applications: [:logger]
    ]
  end

  defp deps do
    [
      {:nimble_csv, "~> 0.3"}
    ]
  end

  defp aliases do
    [
      "compile":    ["compile --warnings-as-errors"]
    ]
  end
end
